FROM node:20-alpine

WORKDIR /usr/src/app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

COPY package*.json /usr/src/app/
RUN npm install
RUN npm install -g mocha
RUN npm install @sentry/node
RUN npm install @sentry/node --save


COPY . /usr/src/app


ENV PORT 5000
EXPOSE $PORT
CMD [ "npm", "start" ]
